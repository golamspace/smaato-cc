--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: req_tracker; Type: TABLE; Schema: public; Owner: smaato
--

CREATE TABLE public.req_tracker (
    id integer NOT NULL,
    request_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.req_tracker OWNER TO smaato;

--
-- Name: req_tracker_id_seq; Type: SEQUENCE; Schema: public; Owner: smaato
--

CREATE SEQUENCE public.req_tracker_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.req_tracker_id_seq OWNER TO smaato;

--
-- Name: req_tracker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smaato
--

ALTER SEQUENCE public.req_tracker_id_seq OWNED BY public.req_tracker.id;


--
-- Name: req_tracker id; Type: DEFAULT; Schema: public; Owner: smaato
--

ALTER TABLE ONLY public.req_tracker ALTER COLUMN id SET DEFAULT nextval('public.req_tracker_id_seq'::regclass);


--
-- Data for Name: req_tracker; Type: TABLE DATA; Schema: public; Owner: smaato
--

COPY public.req_tracker (id, request_id, created_at) FROM stdin;
\.


--
-- Name: req_tracker_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smaato
--

SELECT pg_catalog.setval('public.req_tracker_id_seq', 1, false);


--
-- Name: req_tracker req_tracker_pkey; Type: CONSTRAINT; Schema: public; Owner: smaato
--

ALTER TABLE ONLY public.req_tracker
    ADD CONSTRAINT req_tracker_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

