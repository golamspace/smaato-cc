package com.smaato.smaatocc.controller;

import com.smaato.smaatocc.constant.ApiEndPoints;
import com.smaato.smaatocc.constant.Beans;
import com.smaato.smaatocc.service.RequestCounterService;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.smaato.smaatocc.constant.Common.KEYWORD_FAILED;

@Slf4j
@RequestMapping
@RestController(value = Beans.BEAN_CONTROLLER_REQ_COUNTER)
public class RequestCounterController {

    private final RequestCounterService requestCounterService;

    @Autowired
    public RequestCounterController(RequestCounterService requestCounterService) {
        this.requestCounterService = requestCounterService;
    }

    @GetMapping(value = ApiEndPoints.API_REQ_COUNTER)
    public HttpEntity<String> getCountryRecordList(
            @NotNull @RequestParam(value = "id") Integer id,
            @Nullable @RequestParam(value = "endpoint") String endpoint
    ) {
        log.info("query params,"
                + " id: " + id
                + ", endpoint: " + endpoint
        );

        if (id == null) {
            log.error("Required argument missing: id!");
            return new ResponseEntity<>(KEYWORD_FAILED, HttpStatus.OK);
        }

        String responseBody;
        try {
            responseBody = requestCounterService.getActiveRecordList(id, endpoint);
        } catch (Exception exception) {
            responseBody = KEYWORD_FAILED;
        }
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }
}
