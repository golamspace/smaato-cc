package com.smaato.smaatocc.constant;

public class Beans {
    public static final String BEAN_CONTROLLER_REQ_COUNTER = "reqCounterController";

    public static final String BEAN_REPOSITORY_REQ_TRACKER = "reqTrackerRepository";

    public static final String BEAN_SERVICE_REQ_COUNTER = "reqCounterService";
}
