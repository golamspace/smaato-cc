package com.smaato.smaatocc.constant;

public class Common {
    public static final String API_BASE_PATH = "/api";
    public static final String KEYWORD_OK = "ok";
    public static final String KEYWORD_FAILED = "failed";
    public static final String SMAATO_KAFKA_STREAM_NAME = "smaato-stream";
}
