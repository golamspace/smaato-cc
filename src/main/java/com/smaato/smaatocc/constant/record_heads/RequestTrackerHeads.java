package com.smaato.smaatocc.constant.record_heads;

public class RequestTrackerHeads {
    public static final String REQUEST_TRACKER_HEAD_REQUEST_ID = "request_id";
    public static final String REQUEST_TRACKER_HEAD_CREATED_AT = "created_at";

}
