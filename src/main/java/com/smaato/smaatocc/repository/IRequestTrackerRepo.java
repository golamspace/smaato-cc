package com.smaato.smaatocc.repository;

import com.smaato.smaatocc.constant.Beans;
import com.smaato.smaatocc.model.dao.RequestTrackerDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository(value = Beans.BEAN_REPOSITORY_REQ_TRACKER)
public interface IRequestTrackerRepo extends JpaRepository<RequestTrackerDao, Integer> {
    @Query(
            value = "SELECT COUNT(DISTINCT request_id) FROM req_tracker WHERE created_at >= NOW() - INTERVAL '1 MINUTE'",
            nativeQuery = true
    )
    Integer findRecordCountForLastMinute();

    @Query(value = "SELECT COUNT(*) FROM req_tracker WHERE id > :id GROUP BY request_id;", nativeQuery = true)
    Integer countRecordGreaterThanId(Integer id);
}
