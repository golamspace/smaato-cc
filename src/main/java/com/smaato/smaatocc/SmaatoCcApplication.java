package com.smaato.smaatocc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(
		scanBasePackages = {"com.smaato.smaatocc"},
		exclude = {
				SecurityAutoConfiguration.class
		}
)
@EntityScan("com.smaato.smaatocc")
@EnableScheduling
public class SmaatoCcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaatoCcApplication.class, args);
	}

}
