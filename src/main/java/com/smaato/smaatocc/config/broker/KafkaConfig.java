package com.smaato.smaatocc.config.broker;

import lombok.Data;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.kafka.core.KafkaAdmin;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static com.smaato.smaatocc.constant.Common.SMAATO_KAFKA_STREAM_NAME;

@Data
@Configuration
@ConfigurationProperties("kafka")
public class KafkaConfig {
    @Value("${kafka.bootstrapAddress}")
    private String bootStrapAddress;

    @Value(("${kafka.topicName}"))
    private String topicName;

    private final GenericApplicationContext context;

    public KafkaConfig(GenericApplicationContext context) {
        this.context = context;
    }

    @Data
    public static class KafkaConf {
        private String item;
        private String topic;
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> adminConfigs = new HashMap<>();
        adminConfigs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapAddress);
        return new KafkaAdmin(adminConfigs);
    }

    @PostConstruct
    private void registerTopics() {
        this.context.registerBean(
                SMAATO_KAFKA_STREAM_NAME,
                NewTopic.class,
                () -> new NewTopic(topicName, 1, (short) 1)
        );
    }
}
