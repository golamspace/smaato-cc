package com.smaato.smaatocc.utils;

import com.smaato.smaatocc.utils.cdr.Constant;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import static com.smaato.smaatocc.utils.cdr.Constant.CDR_FILE_DIR_SEPARATOR;
import static com.smaato.smaatocc.utils.cdr.Constant.CDR_ROTATED_FILE_TIME_FORMAT;

@Slf4j
@Component
public class CsvWriter {

    @Synchronized
    public void dumpCdr(String filePathAbsolute, String data, Class<? extends Enum<?>> headerClass) {
        File file = new File(filePathAbsolute);
        BufferedWriter writer;
        CSVPrinter csvPrinter;
        try {
            if (!file.exists()) {
                writer = Files.newBufferedWriter(
                        Paths.get(filePathAbsolute),
                        StandardOpenOption.CREATE
                );
                csvPrinter = new CSVPrinter(
                        writer,
                        CSVFormat.DEFAULT
                                .withHeader(headerClass)
                                .withDelimiter(',')
                                .withEscape('"')
                                .withQuoteMode(QuoteMode.NONE)
                );
                //TODO adding an extra header record, rethink
                csvPrinter.printRecord("");
            } else {
                writer = Files.newBufferedWriter(
                        Paths.get(filePathAbsolute),
                        StandardOpenOption.APPEND
                );
            }

            csvPrinter = new CSVPrinter(
                    writer,
                    CSVFormat.DEFAULT
                            .withDelimiter('|')
                            .withEscape('"')
                            .withQuoteMode(QuoteMode.NONE)
            );

            csvPrinter.printRecord(data);
            csvPrinter.flush();
        } catch (IOException ioException) {
            log.error("Exception at csv write: " + ioException.getCause());
        }
    }

    /**
     * @param filePath - the path of the file
     * @return the file size in Mega Bytes as long value
     * @throws IOException – if an I/O error occurs
     */
    private Long getFileSize(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        long fileSize = Files.size(path);
        return fileSize / (1024 * 1024);
    }

    /**
     * rotates fileName to yyyyMMdd_HHmmss_fileName with current time
     * in the same directory where current file resides
     *
     * @param fileDir  - file directory where current file resides
     * @param fileName - file that has to be renamed
     * @throws IOException – if an I/O error occurs
     */
    private void rotateFile(String fileDir, String fileName) throws IOException {
        String currentFileName = fileDir.concat(CDR_FILE_DIR_SEPARATOR).concat(fileName);
        String newFileName = fileDir
                .concat(CDR_FILE_DIR_SEPARATOR)
                .concat(
                        TimeUtils.getCurrentTimeStampAsFormattedString(CDR_ROTATED_FILE_TIME_FORMAT)
                                + Constant.CDR_FILENAME_SEPARATOR
                                + fileName
                );

        Path currentPath = Paths.get(currentFileName);
        Files.move(currentPath, currentPath.resolveSibling(newFileName));
    }

    /**
     * @param fileDir     - file directory where current file resides
     * @param fileName    - file that has to be renamed
     * @param fileMaxSize - max size of the file after which file will be rotated
     */
    @Synchronized
    public void rotateFileIffRequired(String fileDir, String fileName, long fileMaxSize) {
        try {
            long fileSize = this.getFileSize(fileDir.concat(CDR_FILE_DIR_SEPARATOR).concat(fileName));
            log.info("current size: " + fileSize + ", maxSize: " + fileMaxSize);
            if (fileSize >= fileMaxSize) {
                this.rotateFile(fileDir, fileName);
            }
        } catch (IOException ioException) {
            log.error("Exception at file rotation: " + ioException.getCause());
        }
    }
}
