package com.smaato.smaatocc.utils.cdr;

import org.springframework.stereotype.Component;

@Component
public class CdrFactory {
    public CDR createCdr() {
        return new CDR();
    }
}
