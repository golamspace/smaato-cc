package com.smaato.smaatocc.utils.cdr;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.io.Serializable;

@Getter
@Setter
@Component
@RequestScope
public class CdrContext implements Serializable {
    private CDR cdr;
}
