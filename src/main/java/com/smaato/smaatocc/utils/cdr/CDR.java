package com.smaato.smaatocc.utils.cdr;

import lombok.Setter;

import java.sql.Timestamp;

@Setter
public class CDR {
  private Timestamp logTime;
  private Integer uniqueIdCount;

  @Override
  public String toString() {
    return logTime + "," + uniqueIdCount;
  }
}
