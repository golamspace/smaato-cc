package com.smaato.smaatocc.utils.cdr;

public class Constant {
    public static final String CDR_FILENAME_SEPARATOR                               = "_";
    public static final String CDR_FILE_DIR_SEPARATOR                               = "/";
    public static final String CDR_ROTATED_FILE_TIME_FORMAT                         = "yyyyMMdd_HHmmss";
}
