package com.smaato.smaatocc.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TimeUtils {

    public static Timestamp getCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static String getCurrentTimeStampAsFormattedString() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSS"));
    }

    public static String getStringFromTimeStamp(Timestamp timestamp, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date(timestamp.getTime()));
    }

    public static String getCurrentTimeStampAsFormattedString(String timeFormat) {
        return timeFormat != null && !timeFormat.trim().isEmpty() ? getStringFromTimeStamp(getCurrentTimeStamp(), timeFormat) : getCurrentTimeStampAsFormattedString();
    }
}
