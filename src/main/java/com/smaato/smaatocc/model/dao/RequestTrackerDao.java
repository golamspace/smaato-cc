package com.smaato.smaatocc.model.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

import java.sql.Timestamp;

import static com.smaato.smaatocc.constant.Entities.TABLE_REQ_TRACKER;
import static com.smaato.smaatocc.constant.record_heads.RequestTrackerHeads.*;

@Entity
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = TABLE_REQ_TRACKER)
public class RequestTrackerDao {
    @Id
    @Column(
            columnDefinition = "serial",
            name = "id"
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(
            access = JsonProperty.Access.WRITE_ONLY
    )
    private Integer id;

    @Setter
    @Column(
            columnDefinition = "integer",
            name = REQUEST_TRACKER_HEAD_REQUEST_ID,
            nullable = false
    )
    private Integer requestId;

    @Setter
    @Column(
            columnDefinition = "timestamp",
            name = REQUEST_TRACKER_HEAD_CREATED_AT,
            nullable = false
    )
    private Timestamp createdAt;
}
