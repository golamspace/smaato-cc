package com.smaato.smaatocc.service;

import com.smaato.smaatocc.config.broker.KafkaConfig;
import com.smaato.smaatocc.constant.Beans;
import com.smaato.smaatocc.model.dao.RequestTrackerDao;
import com.smaato.smaatocc.repository.IRequestTrackerRepo;
import com.smaato.smaatocc.utils.CsvWriter;
import com.smaato.smaatocc.utils.broker.EventSender;
import com.smaato.smaatocc.utils.cdr.CDR;
import com.smaato.smaatocc.utils.cdr.CdrFactory;
import com.smaato.smaatocc.utils.cdr.CdrHeader;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.sql.Timestamp;

import static com.smaato.smaatocc.constant.Common.KEYWORD_OK;
import static com.smaato.smaatocc.utils.cdr.Constant.CDR_FILE_DIR_SEPARATOR;

@Slf4j
@Transactional
@Service(value = Beans.BEAN_SERVICE_REQ_COUNTER)
public class RequestCounterService {

    @Value("${cdr.fileDir}")
    private String fileDir;

    @Value("${cdr.fileName}")
    private String fileName;

    @Value("${cdr.fileMaxSize:200}")
    private long fileMaxSize;

    private final IRequestTrackerRepo iRequestTrackerRepo;

    private final KafkaConfig kafkaConfig;

    private final EventSender eventSender;

    private final CdrFactory cdrFactory;

    private final CsvWriter csvWriter;

    private static Integer lastMinuteUniqueRequestCount = 0;

    @Autowired
    public RequestCounterService(
            IRequestTrackerRepo iRequestTrackerRepo,
            KafkaConfig kafkaConfig,
            EventSender eventSender,
            CdrFactory cdrFactory,
            CsvWriter csvWriter
    ) {
        this.iRequestTrackerRepo = iRequestTrackerRepo;
        this.kafkaConfig = kafkaConfig;
        this.eventSender = eventSender;
        this.cdrFactory = cdrFactory;
        this.csvWriter = csvWriter;
    }

    /**
     * counts unique request based on request {{id}}
     *
     * @param requestId mandatory parameter to count accepted request
     * @param endpoint  optional endpoint to call to share current count stat
     * @return status of request processing
     */
    public String getActiveRecordList(@NotNull Integer requestId, @Nullable String endpoint) {
        log.info("Adding new record record found with request id: " + requestId);

        log.info("Last minute unique request id count: " + lastMinuteUniqueRequestCount);

        RequestTrackerDao requestTrackerDao = RequestTrackerDao
                .builder()
                .requestId(requestId)
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .build();
        iRequestTrackerRepo.save(requestTrackerDao);

        if (endpoint != null && !endpoint.isEmpty()) {
            String responseBody = WebClient.builder().build()
                    .get()
                    .uri(endpoint)
                    .attribute("count", lastMinuteUniqueRequestCount)
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
            log.info("response from endpoint " + endpoint + " : " + responseBody);
        }

        return KEYWORD_OK;
    }

    @Scheduled(fixedRate = 60000)
    public void updateRequestCounter() {
        CDR cdr = cdrFactory.createCdr();
        Integer uniqueOccurrenceLastMinute = iRequestTrackerRepo.findRecordCountForLastMinute();
        log.info("total unique id logged in last minute: " + uniqueOccurrenceLastMinute);
        lastMinuteUniqueRequestCount = uniqueOccurrenceLastMinute;

        cdr.setLogTime(new Timestamp(System.currentTimeMillis()));
        cdr.setUniqueIdCount(uniqueOccurrenceLastMinute);

        String cdrString = cdr.toString();
        String filePathAbsolute = fileDir.concat(CDR_FILE_DIR_SEPARATOR).concat(fileName);
        csvWriter.dumpCdr(filePathAbsolute, cdrString, CdrHeader.class);
        csvWriter.rotateFileIffRequired(fileDir, fileName, fileMaxSize);

        String topicName = kafkaConfig.getTopicName();
        if (topicName == null || topicName.isEmpty()) {
            log.error("No proper topic name found to stream cdr!");
        } else {
            eventSender.send(topicName, cdrString);
        }
    }
}
