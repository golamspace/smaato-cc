from flask import Flask, json, request

api = Flask(__name__)
  
@api.route('/dummy/endpoint', methods=['GET'])
def mnp_test():
  print("request-body ", request.data)
  print("request-param ", request.args)
  print("request-headers ", request.headers)
 
  return json.dumps("ok"), 200 , {'Content-Type' : 'application/text'}

if __name__ == '__main__':
    api.run(debug=True, port=8080) 
    
    
