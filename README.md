# smaato-cc
This service is responsible to accept id wise requests.

### HLD

![alt text](<./assets/smaato-cc-hld.png>)

### Prerequisites
* openjdk 11.0.13 installed
* postgres-13 installed
* kafka(along with zookeeper) installed
* git client installed
* python3 installed (optional, iff want to check endpoint with dummy api server)

### Clone Repo
Copy the project url and clone using _git_:

```shell
git clone git@gitlab.com:golamspace/smaato-cc.git
```

or
```shell
git clone https://gitlab.com/golamspace/smaato-cc.git
```

### Environment
create an _.env_ file
```shell
cd smaato-cc
touch .env
```

open the _.env_ file using _vim_ or _nano_ or any text editor and add the following lines:
Following environment vars with sample values are to be set prior to run the app. Please change values accordingly:
```shell
export DATASOURCE_URL=jdbc:postgresql://localhost:5432/smaato_cc
export DATASOURCE_USERNAME=smaato
export DATASOURCE_PASSWORD=smaato@123
export CDR_FILE_DIR=/usr/local/tomcat/logs/smaato
export CDR_FILE_NAME=smaatoCdr.csv
export CDR_FILE_MAX_SIZE=20
export KAFKA_HOST=127.0.0.1
export KAFKA_PORT=9092
export KAFKA_TOPIC=smaato-stream
```

### Prepare Database
Considering you have postgres-13 installed and running on _5432_ port of _localhost_:
```shell
psql -U postgres -d postgres
create user smaato with encrypted password 'smaato@123';
create database smaato_cc owner smaato;
```

now as we have our db created we need to load the schema(kept in project _resource_ directory--> resources/smaato_cc.sql):

```shell
psql -U smaato -d smaato_cc < smaato_cc.sql
```

### Creating CDR folder
CDR directory has to be created so that application can dump cdr files there,
say iff you intend to dump all cdrs in the path shown above in _CDR_FILE_DIR_
which is _/usr/local/tomcat/logs/smaato_ do the following:

```shell
mkdir /usr/local/tomcat/logs/smaato
```

### Run Application
Load the _.env_ file and run _gradlew_:
```shell
cd smaato-cc
source .env
./gradlew clean bootRun
```

and to build war file in _smaato-cc/build/libs_ directory:
```shell
cd smaato-cc
./gradlew clean bootWar 
```

### Test Application
Run the postman collection provided in _assets_ folder,
don't forget to change the _hostname_ variable in postman collection
as per the ip/port you assigned the _server.ip_ and _server.port_ variable in _application.properties_ file.
Iff you don't change what is kept here, _hostname_ will be _127.0.0.1:8085_.

Optionally iff we want to test the api with optional query parameter _endpoint_ set to
_http://localhost:8080/dummy/endpoint_ for instance like shown in the postman collection,
run the python script _dummy_endpoint_api_server.py_ kept in _assets_ directory like below:

```shell
python3 dummy_endpoint_api_server.py
```

and then run the request that contains both _id_ and _endpoint_ set from the postman collection provided.